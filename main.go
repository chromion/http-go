package main

import (
	"flag"
	"net/http"
)

func main() {
	var port string
	flag.StringVar(&port, "p", "8080", "port")

	var cors bool
	flag.BoolVar(&cors, "c", false, "cors")

	flag.Parse()

	path := flag.Args()[0]

	http.Handle("/", handler(http.FileServer(http.Dir(path)), cors))
	if err := http.ListenAndServe(":" + port, nil); err != nil {
		panic(err)
	}
}

func handler(h http.Handler, cors bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// set some header
		if cors == true {
			w.Header().Set("Access-Control-Allow-Origin", "*")
		}
		// serve with header
		h.ServeHTTP(w, r)
	}
}

