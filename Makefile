TAG?=latest

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o http-go .
build-docker:
	docker build -t registry.gitlab.com/chromion/http-go:${TAG} .
push-docker:
	docker push registry.gitlab.com/chromion/http-go:${TAG}

