# Minimal webserver written in Go

```http-go``` is a simple http server written in go. It's intended for testing purposes only. **Don't use this in production.**

## Usage:

```
http-go [arguments] path
```

```path``` is required.

## Available Options:

```-p``` Port to use (defaults to 8080)

```-c``` Enable CORS
